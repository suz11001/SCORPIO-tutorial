# SCORPiOs - Synteny-guided CORrection of Paralogies and Orthologies Tutorial

This tutorial is a adapted from the original git that can be found [here](https://github.com/DyogenIBENS/SCORPIOS) and is designed for using the computing cluster Xanadu at UCONN. 

 SCORPiOs is a **synteny-guided gene tree correction pipeline** for clades that have undergone a whole-genome duplication event. SCORPiOs identifies gene trees where the whole-genome duplication is **missing** or **incorrectly placed**, based on the genomic locations of the duplicated genes across the different species. SCORPiOs then builds an **optimized gene tree** consistent with the known WGD event, the species tree, local synteny context, as well as gene sequence evolution.

 SCORPiOs is implemented as a Snakemake pipeline. SCORPiOs takes as input either gene trees or multiple alignments, and outputs the corresponding optimized gene trees.

 A **detailed** documentation of this software can be found [here](https://scorpios.readthedocs.io/en/latest/)!

![SCORPiOs illustrated](<./scorpios_illustrated.png>) 

If you use SCORPiOs, please cite:

Parey E, Louis A, Cabau C, Guiguen Y, Roest Crollius H, Berthelot C, Synteny-guided resolution of gene trees clarifies the functional impact of whole genome duplications, Molecular Biology and Evolution, msaa149, https://doi.org/10.1093/molbev/msaa149.

# Quick start

## Table of content
  - [Installation](#installation)
    - [Installing conda](#installing-conda)
    - [Installing SCORPiOs](#installing-scorpios)
  - [Usage](#usage)
    - [Setting up your working environment for SCORPiOs](#setting-up-your-working-environment-for-scorpios)
    - [Running SCORPiOs on example data](#running-scorpios-on-example-data)
      - [Example 1: Simple SCORPiOs run](#example-1-simple-scorpios-run)
    - [Running SCORPiOS on your data](#running-scorpios-on-your-data)
      - [Preparing your configuration file](#preparing-your-configuration-file)
      - [Running SCORPiOs](#running-scorpios)

## Installation

### Installing conda

- Global Conda Installation

  You do not have to install anaconda. Instead at command line (or in a shell script) run the following:
  `module load anaconda`

### Installing SCORPiOs

- Clone the repository and go to SCORPiOs root folder
  ```
  git clone SCORPIO-tutorial
  cd SCORPIO-tutorial
  ```
- Create the main conda environment.

  While you have the anaconda module still loaded, you wanted to create the environment as described in the [envs/scorpios.yaml](envs/scorpios.yaml).  
  I have changed the scorpios.yaml so that it will actually create the environment without any hiccups.

  ```
  conda env create -f envs/scorpios.yaml
  ```
  **FYI** environments by default are created in: `~/.conda/envs/`

## Usage

### Setting up your working environment for SCORPiOs

Before any SCORPiOs run, you should:
 - go to SCORPiOs root folder,
 - make sure you still have anaconda loaded
 - activate the conda environment with `source activate scorpios`.
 - unload the anaconda module `module unload anaconda`

### Running SCORPiOs on example data

Lets run SCORPIO on example data provided by the authors.

#### Example 1: Simple SCORPiOs run

SCORPiOs uses a YAML configuration file to specify inputs and parameters for each run.
An example configuration file is provided: [config_example.yaml](config_example.yaml). This configuration file executes SCORPiOs on toy example data located in [data/example/](data/example/), that you can use as reference for input formats.

The only required snakemake arguments to run SCORPiOs are `--configfile` and the `--use-conda` flag. Optionally, you can specify the number of threads via the `--cores` option. For more advanced options, you can look at the [Snakemake documentation](https://snakemake.readthedocs.io/en/stable/).

To run SCORPiOs on example data:

```
snakemake --configfile config_example.yaml --use-conda --cores 4
```

The following output should be generated in [SCORPiOs_example](SCORPiOs_example/):
`SCORPiOs_example/SCORPiOs_output_0.nhx`.

### Running SCORPiOs on your data

#### Preparing your configuration file
To run SCORPiOs on your data, you have to create a new configuration file for your SCORPiOs run. You will need to format your input data adequately and write your configuration file, using the provided example [config_example.yaml](config_example.yaml) as a guide.

- Copy the example config file `cp config_example.yaml config.yaml`
- Open and edit `config.yaml` to specify paths, files and parameters for your data
- The most important parameters is specify WGDs and outgroups, this is how you do it described [here](https://scorpios.readthedocs.io/en/latest/input_description.html)

To check your configuration, you can execute a dry-run with `-n`.
```
snakemake --configfile config.yaml --use-conda -n
```

#### Running SCORPiOs
Finally, you can run SCORPiOs as described above:

```
snakemake --configfile config.yaml --use-conda
```

or in iterative mode:

```
bash iterate_scorpios.sh --snake_args="--configfile config.yaml"
```
